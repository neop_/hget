package main

import (
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"

	"golang.org/x/net/html"
)

var base_path string
var actively_crawled map[string]bool
var max_depth int = 2
var keep_hostname bool = true
var httpget_retries int = 10

func crawl_html(node *html.Node, action func(string)) {
	if node.Type == html.ElementNode && node.Data == "a" {
		for _, attribute := range node.Attr {
			if attribute.Key == "href" {
				action(attribute.Val)
				break
			}
		}
	}
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		crawl_html(child, action)
	}
}


func recursive_download(base *url.URL, href string, depth int) {
	ref, err := url.Parse(href)
	if err != nil {
		log.Println(err)
		return
	}

	url := base.ResolveReference(ref)
	// fragments (#asdf) don't matter to us
	url.Fragment = ""

	if keep_hostname && ref.Hostname() != "" && base.Hostname() != ref.Hostname() {
		return
	}

	escaped_path := url.EscapedPath()
	relative_file_path := path.Join(url.Hostname(), escaped_path)
	// TODO: detect if the path is a directory (literally impossible)
	// if escaped_path[len(escaped_path) - 1] == '/' {
	relative_file_path = path.Join(relative_file_path, "index.html")
	// }

	if actively_crawled[relative_file_path] {
		return
	}
	actively_crawled[relative_file_path] = true
	defer delete(actively_crawled, relative_file_path)
	
	hget_data_file_path := path.Join(base_path, "hget_data", relative_file_path)
	website_mirror_file_path := path.Join(base_path, "website", relative_file_path)
	
	// abort if already downloaded this ref
	if _, err := os.Stat(hget_data_file_path); !os.IsNotExist(err) {
		return
	}

	log.Println(strings.Repeat(" ", depth) + relative_file_path + " {")

	os.MkdirAll(path.Dir(website_mirror_file_path), os.ModeDir | 0755)
	body_file, err := os.Create(website_mirror_file_path)
	if err != nil {
		log.Fatal(err)
	}
	defer body_file.Close()


	html_doc, err := perform_request(url, body_file)
	for retries := 1; err != nil; retries++ {
		if retries > httpget_retries {
			log.Fatalf("request failed too often, last error: %v\n", err)
		}

		log.Printf("request failed on attempt %v with error: %v\nretrying...\n", retries-1, err)
		html_doc, err = perform_request(url, body_file)
	}
	
	if depth < max_depth {
		crawl_html(html_doc, func(href string) { recursive_download(url, href, depth + 1) })
	}
	
	// mark downloaded by creating a meta file
	if err := os.MkdirAll(path.Dir(hget_data_file_path), os.ModeDir | 0755); err != nil {
		log.Fatal(err)
	}
	
	if _, err = os.Create(hget_data_file_path); err != nil {
		log.Fatal(err)
	}

	log.Println(strings.Repeat(" ", depth) + "}")
}

func perform_request(url *url.URL, body_file *os.File) (*html.Node, error) {
	response, err := http.Get(url.String())
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	html_doc, err := html.Parse(io.TeeReader(response.Body, body_file))
	if err != nil {
		return nil, err
	}

	return html_doc, nil
}


func main() {
	base, err := url.Parse(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	base_path = os.Args[2]
	actively_crawled = make(map[string]bool)
	recursive_download(base, ".", 0)
}
